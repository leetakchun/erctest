//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
// import "@openzeppelin/contracts/utils/math/Math.sol";
import "hardhat/console.sol";

// 1- Make an ERC20 token with fees.
// a) Part of the fees taken should be burned, the other part is sent to a fund.
// b) Implement blacklisting

contract ERC20Token is ERC20, Ownable, ReentrancyGuard{
    using SafeMath for uint256;

    /* ========== STATE VARIABLES ========== */
    uint256 public maxSupply = 123456789; 
    uint256 public minCost = 1 ether;
    uint public ethFund;
    address payable public contractAddress;
    uint private commissionRatio = 3;

    mapping(address => bool) private blacklisting;


    constructor()  ERC20("exercise 1", "EX1") payable{
        contractAddress= payable(address(this));
    }

    function payment() public payable returns(bool){
        require(msg.value >= minCost, "ether not enough");
        return true;
    }

    function burnEth(uint _amount) private returns(bool){
        (bool sent, bytes memory data) = address(0).call{value:_amount}(""); //send eth to 0x0 for burn eth
        require(sent, "burn failed");
        console.log("data burnEth() ", string(data));
        return sent;
    }

    function mintCoin(uint256 amount) public payable nonReentrant{
        require(payment());
        require(totalSupply() + amount <= maxSupply); //set total supply of token
        require(blacklisting[msg.sender] != true, "blacklist occur");
        uint256 receiveEth = msg.value;
        console.log("contract balance before burn", contractAddress.balance);
        burnEth(receiveEth.div(3)); //burn eth
        ethFund = contractAddress.balance; //update ethfund
        console.log("contract balance AFTER burn", contractAddress.balance);
        _mint(msg.sender, amount); 
    }

    //get the eth value of this contract
    function getBalance() public view returns (uint){
        return address(this).balance;
    }

    //blacklist checking, onlyOwner
    function setBlacklist(address blacklist) public virtual onlyOwner{
        blacklisting[blacklist] = true;
    }

    function removeBlacklist(address blacklist) public virtual onlyOwner{
        blacklisting[blacklist] = false;
    }

    function getBlacklist(address blacklist) public view virtual onlyOwner returns(bool){
        return blacklisting[blacklist];
    }

    function sendEth(address payable userAddress, uint _amount)public payable virtual onlyOwner returns(bool){
        (bool sent, bytes memory data) = userAddress.call{value: _amount}("");
        require(sent, "failed to send ether");
    }

    function withdrawAllEths() public payable virtual onlyOwner returns(bool) {
        require(payable(msg.sender).send(address(this).balance), "withdraw failed"); //send ETH to msg.sender(owner) from address(this)
        return true;
    }

    // Function to receive Ether. msg.data must be empty
    receive() external payable {
        console.log("hello receive() function called");
    }

    // Fallback function is called when msg.data is not empty
    fallback() external payable {}


}


