//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "hardhat/console.sol";

contract RewardToken is ERC20, Ownable{
    uint256 public maxSupply = 123456789; 
    uint256 public minCost = 0.1 ether;
    IERC20 public stakingToken;

    constructor(address _stakingToken) ERC20("RewardToken", "RDT"){
        stakingToken = IERC20(_stakingToken); //initial the contract
        _mint(msg.sender, maxSupply); //mint all the coins from user;
    }

    function transfer(address to, uint256 amount) public virtual override returns (bool){
        // console.log("stakingToken.balanceOf(to) > ", stakingToken.balanceOf(to));
        require(stakingToken.balanceOf(to) > 0, "Current Address no staking Token stake into contract"); //need user has balance in the StakingToken contract
        address creator = owner();
        transferFrom(creator, to, amount);
        return true;
    }

    function transferFrom( address sender, address recipient, uint amount) public virtual override returns (bool){
        //add checking for specific user
        bool success = super.transferFrom(sender, recipient, amount); //using parent function
        console.log("exec override transferFrom() success");
        return success;
    }

}


