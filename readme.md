Since the .sol file is run on the Remix, if you need to run it locally please change the contract import path to "./node_modules/"

The ERC20_EX-stakeToken.sol is the 1st test, make with fee, part of fee is burn and part is store in fund(contract), blacklist is established

In the 2nd test, I have correct the mistakes in the following:
line:85, uint is changed to 1e18 which same as the unit of eth
line:86, the periodFinish is changed to _totalSupply
line:96, unit is changed to 1e18 which same as the unit of eth
line:100 to 107: update the Two function
line:144 to 155 there is a duplicate function for the range of the lockingTimeStamp[msg.sender]